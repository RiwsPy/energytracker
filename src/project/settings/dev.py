from .common import *  # noqa: F401

DEBUG = True
DEBUG_TOOLBAR = False
SECRET_KEY = "django-insecure-pj%@pap&655rnnzb+*=twvch5do-j#$ct(l^s+um$&m@8z9_)%"
ALLOWED_HOSTS = ["127.0.0.1", "localhost"]

if DEBUG_TOOLBAR:
    INSTALLED_APPS.append("debug_toolbar")
    MIDDLEWARE.append("debug_toolbar.middleware.DebugToolbarMiddleware")
    INTERNAL_IPS.append("127.0.0.1")
