from django.conf import settings
from django.urls import include, path

urlpatterns = [
    path("django_plotly_dash/", include("django_plotly_dash.urls")),
    path("", include("energytracker.urls")),
]

if settings.DEBUG_TOOLBAR:
    urlpatterns.insert(0, path("__debug__/", include("debug_toolbar.urls")))
