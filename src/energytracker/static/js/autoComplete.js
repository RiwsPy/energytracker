function getCookie(name) {
  if (document.cookie && document.cookie !== '') {
    let cookies = document.cookie.split(';')
    for (let cookie of cookies) {
      let [cookieName, cookieValue] = cookie.trim().split('=')
      if (cookieName === name) {
        return decodeURIComponent(cookieValue)
      }
    }
  }
  return null
}
const csrftoken = getCookie('csrftoken');

function request(id) {
  const url = new URL(`${window.location.href}${id}/`)
  const request = new Request(url, {
    method: 'POST',
    mode: 'same-origin',
    headers: new Headers({ "X-CSRFToken": csrftoken }),
  })

  try {
    fetch(request)
      .then(response => response.text())
      .then(data => {
        const resultBlock = document.querySelector("#compare-results");
        resultBlock.innerHTML = data
      })
  } catch (error) {
    console.error(error)
  }
}

const clearButton = document.getElementById('clear-button');
const autoCompleteJS = new autoComplete({
  data: {
    src: async () => {
      const root = document.getElementById("autoComplete");
      root.setAttribute("placeholder", "Chargement...");
      const api_url = window.location.origin + "/api/naf_desc/";
      const request = new Request(api_url, {
        method: 'GET',
        mode: 'same-origin',
        headers: new Headers({
          'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
        })
      })
      try {
        const source = await fetch(request);
        const data = await source.json();
        root.setAttribute("placeholder", autoCompleteJS.placeHolder);
        return data["data"];
      } catch (error) {
        return error;
      }
    },
    keys: ["id", "label", "keywords"],
    cache: true,
    filter: (list) => {
      // Filter duplicates
      // incase of multiple data keys usage
      let filteredResults = Array()
      for (let value of list) {
        if (value.key === "keywords") {
          value.match = value.value.label;
          value.key = "label";
        }
        filteredResults.push(value);
      }
      filteredResults = Array.from(
        new Set(filteredResults.map((value) => value.value.label))
      ).map((word) => {
        return filteredResults.find((value) => value.value.label === word);
      });

      return filteredResults;
    }
  },
  placeHolder: "Recherche par mot-clé ou code NAF",
  threshold: 0,
  resultsList: {
    element: (list, data) => {
      const info = document.createElement("p");
      info.classList.add("header_result");
      if (data.results.length > 0) {
        let plural_letter = data.results.length > 1 ? "s" : "";
        info.innerHTML = `<strong>${data.results.length}</strong> résultat${plural_letter} sur <strong>${data.matches.length}</strong>`;
      } else {
        info.innerHTML = `<strong>Aucun</strong> résultat trouvé pour <strong>"${data.query}"</strong>`;
      }
      list.prepend(info);
    },
    noResults: true,
    maxResults: 15,
    tabSelect: true
  },
  resultItem: {
    element: (item, data) => {
      let label = data.key === "id" ? data.value.label : `Code Naf ${data.value.id}`
      item.innerHTML = `
        <span class="result_value">${data.match}</span>
        <span class="result_category">${label}</span>`;
    },
    highlight: true
  },
  events: {
    input: {
      selection: (event) => {
        autoCompleteJS.input.value = event.detail.selection.value.label;
        request(event.detail.selection.value.id)
      },
      focus: () => {
        autoCompleteJS.start();
      },
    }
  }
});

// Effacement du contenu de la barre de recherche lorsque le bouton clear est cliqué
clearButton.addEventListener('click', function () {
  autoCompleteJS.input.value = '';
  autoCompleteJS.input.focus();
});