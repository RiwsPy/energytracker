from django.urls import path

from .views import AboutView, HomeView, NafDescView

urlpatterns = [
    path("", HomeView.as_view(), name="home"),
    path("qui-sommes-nous/", AboutView.as_view(), name="about"),
    path("api/naf_desc/", NafDescView.as_view(), name="api"),
    path("<str:pk>/", HomeView.as_view(), name="home"),
]
