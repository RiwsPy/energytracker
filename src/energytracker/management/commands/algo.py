from importlib import import_module

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Run algorithm"
    class_name = "Cross"

    def add_arguments(self, parser):
        parser.add_argument("filename", nargs="*", type=str, help="Ficher(s) algo ciblé(s)")

    def handle(self, *args, **options) -> None:
        for db_arg in options.get("filename"):
            py_file = self.clean_filename(db_arg)
            algo_class = self._file_to_cls(py_file)
            if algo_class:
                algo_class(**options).do()

    def clean_filename(self, filename: str, suffix: str = ".py") -> str:
        return filename.removesuffix(suffix).replace("/", ".")

    def _file_to_cls(self, filename: str):
        try:
            return getattr(import_module(filename), self.class_name)
        except AttributeError:
            print(f"Class {filename}.{self.class_name} not found.")
        except ModuleNotFoundError:
            print(f"Module {filename} not found.")
