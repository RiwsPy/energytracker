"""
Created on Thu Feb 29 13:38:01 2024

@author: Elise
"""

import os

import dash
from dash import dcc, html
from dash.dependencies import Input, Output, State

# from dash.exceptions import PreventUpdate
import geopandas as gpd
import pandas as pd
import plotly.express as px


# Fix pour pouvoir utliser le fichier depuis plotly sans importer Django
class FakeSettings:
    from pathlib import Path

    BASE_DIR = Path(__file__).resolve().parent.parent.parent
    DB_DIR = os.path.join(BASE_DIR, "db")


try:
    from django.conf import settings

    from django_plotly_dash import DjangoDash as Dash
except ImportError:
    from dash import Dash
    import plotly.io as pio

    settings = FakeSettings()
    pio.renderers.default = "browser"

# end fix

# ---------Imports data

# Ici, on trouve le dossier `db`, qui contient les datas (identique pour tous)
# mettre `mon/dossier/db` peut provoquer des incompatibilités entre les différents OS (windows et linux notamment)
# Il est préférable d'utiliser `os.path.join("mon", "dossier", "db")`
path = settings.DB_DIR


# --données de conso par NAF et EPCI
data = pd.read_csv(
    os.path.join(path, "data_conso.csv"),
    sep=";",
    dtype={"CODE_EPCI_CODE": str},
)


switcher_grands_secteurs = {
    "A": "Agriculture",
    "I": "Industrie",
    "R": "Résidentiel",
    "T": "Tertiaire",
    "X": "Non mentionné",
}

switcher_grands_secteurs_col = {
    "A": "green",
    "I": "darkorange",
    "R": "violet",
    "T": "dodgerblue",
    "X": "dimgray",
}

switcher_range_color = {
    "A": "Greens",
    "I": "Oranges",
    "R": "PuRd",
    "T": "Blues",
    "X": "Greys",
}


# --Conso par grand secteur
df_conso_gd_secteur = data.groupby("CODE_GRAND_SECTEUR")["CONSO"].sum().reset_index()
df_conso_gd_secteur["CONSO"] = df_conso_gd_secteur["CONSO"].apply(round)
# Calculate the percentage and format it as text
df_conso_gd_secteur["Percentage"] = (
    df_conso_gd_secteur["CONSO"] / df_conso_gd_secteur["CONSO"].sum() * 100
).round().astype(str) + "%"


df_conso_naf = (
    data.groupby(["CODE_GRAND_SECTEUR", "CODE_SECTEUR_NAF1_LIBELLE"])["CONSO"].sum().reset_index()
)

# --Conso par grand secteur et EPCI
df_conso_gd_secteur_epci = (
    data.groupby(
        ["CODE_GRAND_SECTEUR", "LABEL_GRAND_SECTEUR", "CODE_EPCI_CODE", "CODE_EPCI_LIBELLE"]
    )["CONSO"]
    .sum()
    .reset_index()
)
df_conso_gd_secteur_epci["CONSO"] = df_conso_gd_secteur_epci["CONSO"].apply(lambda c: round(c))


# ---Carte de la conso par secteur et par EPCI + Carte de la part de chaque secteur dans la conso par EPCI

conso_tot_epci = (
    df_conso_gd_secteur_epci.groupby("CODE_EPCI_CODE")["CONSO"]
    .sum()
    .reset_index()
    .rename(columns={"CONSO": "Conso tot"})
)


# --Liste des EPCI de région AuRA
epci_aura = pd.read_csv(
    os.path.join(path, "liste_epci_aura.csv"),
    sep=";",
    header=None,
    dtype={0: str},  # liste de strings
)
epci_aura = epci_aura[0].to_list()


map_epci = Dash("map_epci", external_stylesheets=["/static/css/plotly.css"])

# Mise en page du tableau de bord
map_epci.layout = html.Div(
    [
        dcc.Input(id="context", type="hidden", value=""),
        html.Div(
            children=[
                html.Div(
                    children=[
                        html.Label(
                            "Sélectionnez un secteur d'activité ou cliquez sur le secteur qui vous intéresse"
                        ),
                        html.Label(
                            "Sélectionnez une collectivité territoriale ou cliquez sur la carte"
                        ),
                    ],
                    className="selector-labels",
                ),
                html.Div(
                    children=[
                        dcc.Dropdown(
                            id="secteur-selector",
                            options=[
                                {"label": switcher_grands_secteurs[epci], "value": epci}
                                for epci in df_conso_gd_secteur_epci["CODE_GRAND_SECTEUR"].unique()
                            ],
                            placeholder="Secteur d'activité…",
                            # value="T",
                        ),
                        dcc.Dropdown(
                            id="epci-selector",
                            options=[
                                {"label": label, "value": value}
                                for label, value in data[["CODE_EPCI_LIBELLE", "CODE_EPCI_CODE"]]
                                .drop_duplicates()
                                .itertuples(index=False)
                            ],
                            placeholder="Collectivité territoriale…",
                            # value="200040715",  # "Grenoble-Alpes-Métropole",
                        ),
                    ],
                    className="selectors",
                ),
                html.Div(
                    children=[
                        # Map pour la consommation par grand secteur
                        dcc.Graph(id="choropleth-mapbox"),
                        # Sélecteur pour filtrer le Pie Chart
                        dcc.Graph(id="map-pie-chart"),
                    ],
                    className="graphs graphs1",
                ),
                # html.P(
                #     "Cliquez sur un sous-secteur d'activité :",
                #     className="selector-labels",
                # ),
                html.Div(
                    children=[
                        # Barplot pour la consommation d'un grand secteur par code NAF 1
                        dcc.Graph(id="bar-plot"),
                        # Barplot pour la consommation d'un code NAF 1 par code NAF 2
                        dcc.Graph(id="bar-plot2"),
                    ],
                    className="graphs graphs2",
                ),
            ],
            className="graph-container",
        ),
    ]
)

df_epci = gpd.read_file(os.path.join(path, "epci_4326.shp"))
df_epci["CODE_EPCI"] = df_epci["CODE_EPCI"].astype(int).astype(str)
df_epci["LAT_EPCI"] = df_epci["geometry"].apply(lambda pol: pol.centroid.y)
df_epci["LON_EPCI"] = df_epci["geometry"].apply(lambda pol: pol.centroid.x)

# Initialisation du carré blanc pour barplot2
initial_blank_figure = px.bar()
initial_blank_figure.update_layout(plot_bgcolor="white")
initial_blank_figure.update_xaxes(visible=False)
initial_blank_figure.update_yaxes(visible=False)


# Sauvegarde les cartes dans le cache afin d'accélérer leur chargement
MAP_CACHE = dict()


def create_map(code_epci=None, code_secteur=None):
    if (code_epci, code_secteur) in MAP_CACHE:
        return MAP_CACHE[(code_epci, code_secteur)]

    code_secteur = code_secteur or "T"
    if code_epci:
        lat_map, lon_map = df_epci[df_epci["CODE_EPCI"] == code_epci][
            ["LAT_EPCI", "LON_EPCI"]
        ].values[0]
        z = 8
    else:
        lat_map = 46.9
        lon_map = 3
        z = 4

    df_secteur_carte = df_conso_gd_secteur_epci[
        df_conso_gd_secteur_epci["CODE_GRAND_SECTEUR"] == code_secteur
    ]
    df_secteur_carte = df_secteur_carte.merge(
        df_epci[["CODE_EPCI", "NOM_EPCI", "geometry"]],
        how="left",
        left_on="CODE_EPCI_CODE",
        right_on="CODE_EPCI",
    )
    df_secteur_carte = df_secteur_carte.dropna(subset=["geometry"])
    df_secteur_carte = df_secteur_carte.set_geometry("geometry")
    df_secteur_carte = df_secteur_carte.merge(conso_tot_epci, how="left")
    df_secteur_carte["Part consommation (%)"] = round(
        df_secteur_carte["CONSO"] / df_secteur_carte["Conso tot"] * 100
    )
    df_secteur_carte["CONSO_FORMATTED"] = df_secteur_carte["CONSO"].apply(
        lambda x: "{:,.0f}".format(x).replace(",", " ")
    )
    secteur_label = switcher_grands_secteurs[code_secteur]
    secteur_couleur = switcher_range_color[code_secteur]

    fig = px.choropleth_mapbox(
        df_secteur_carte,
        geojson=df_secteur_carte["geometry"],
        locations=df_secteur_carte.index,
        color="Part consommation (%)",
        color_continuous_scale=secteur_couleur,
        labels={
            "Part consommation (%)": "Part de la <br>consommation <br>d'électricité <br>par rapport aux <br>autres secteurs (%)"
        },
        mapbox_style="carto-positron",
        opacity=0.5,
        custom_data=[
            "NOM_EPCI",
            "LABEL_GRAND_SECTEUR",
            "CONSO_FORMATTED",
            "Part consommation (%)",
            "CODE_EPCI",
        ],
        zoom=z,
        center={"lat": lat_map, "lon": lon_map},
        title=f"Part du secteur <b>{secteur_label.lower()}</b> dans la consommation d'électricité <br>par collectivité (en %)",
    )
    fig.update_traces(
        hovertemplate="<br>".join(
            [
                "<b>%{customdata[0]}</b>",
                "Consommation - %{customdata[1]} : %{customdata[2]} MWh",
                "Part de la consommation - %{customdata[1]} : %{customdata[3]} %",
            ]
        )
    )

    # pour espacer le titre de la carte au même niveau que le pie chart
    fig.update_layout(title={"y": 0.95})

    MAP_CACHE[(code_epci, code_secteur)] = fig
    return fig


@map_epci.callback(
    Output("choropleth-mapbox", "figure"),
    Input("epci-selector", "value"),
    Input("secteur-selector", "value"),
)
def update_map(code_epci, code_secteur):
    return create_map(code_epci, code_secteur)


@map_epci.callback(
    Output("map-pie-chart", "figure"),
    Input("epci-selector", "value"),
)
def update_map_pie(code_epci):
    code_epci = code_epci or "200040715"  # GAM

    df_secteur_carte_conso = df_conso_gd_secteur_epci[
        df_conso_gd_secteur_epci["CODE_EPCI_CODE"] == code_epci
    ]

    df_secteur_carte_conso["CONSO_FORMATTED"] = df_secteur_carte_conso["CONSO"].apply(
        lambda x: "{:,.0f}".format(x).replace(",", " ")
    )

    try:
        epci_label = df_secteur_carte_conso["CODE_EPCI_LIBELLE"].iloc[0]
    except IndexError:
        epci_label = "Inconnue"

    fig = px.pie(
        df_secteur_carte_conso,
        values="CONSO",
        names="LABEL_GRAND_SECTEUR",
        color="CODE_GRAND_SECTEUR",
        color_discrete_map=switcher_grands_secteurs_col,
        category_orders={"LABEL_GRAND_SECTEUR": list(switcher_grands_secteurs.values())},
        title=f"Répartition de la consommation d'électricité de la collectivité <br><b>{epci_label}</b>",
        custom_data=["LABEL_GRAND_SECTEUR", "CODE_EPCI_LIBELLE", "CONSO_FORMATTED"],
    )

    fig.update_traces(
        hovertemplate="<br>".join(
            [
                "<b>%{customdata[0][0]}</b>",
                "<i>%{customdata[0][1]}</i>",
                "Consommation : %{customdata[0][2]} MWh",
            ]
        ),
        textposition="outside",
        textinfo="percent+label",
        texttemplate="%{label} <br>%{percent:.1%}",
    )

    fig.update_layout(title={"y": 0.95})  # pour espacer le titre du pie chart

    return fig


# Callback pour mettre à jour le Barplot en fonction de la sélection du Pie Chart
@map_epci.callback(
    Output("bar-plot", "figure"),
    Input("epci-selector", "value"),
    Input("secteur-selector", "value"),
    Input("bar-plot", "clickData"),
)
def update_bar_plot(code_epci, code_secteur, click_data_bar):
    code_epci = code_epci or "200040715"  # GAM

    # si on est sur non mentionné, ou sur résidentiel + hors région AuRA, ne pas afficher le barplot (pas de données détaillées par usage hors AuRA)
    if (code_secteur == "X") | ((code_secteur == "R") & (code_epci not in epci_aura)):
        return initial_blank_figure

    if code_secteur:
        filtered_df = data[
            (data["CODE_EPCI_CODE"] == code_epci) & (data["CODE_GRAND_SECTEUR"] == code_secteur)
        ]
        secteur_label = switcher_grands_secteurs[code_secteur]
    else:
        filtered_df = data[data["CODE_EPCI_CODE"] == code_epci]
        secteur_label = ": Tous"

    try:
        epci_label = filtered_df["CODE_EPCI_LIBELLE"].iloc[0]
    except IndexError:
        epci_label = "Inconnu"

    df_conso_naf = (
        filtered_df.groupby(
            [
                "CODE_EPCI_LIBELLE",
                "CODE_GRAND_SECTEUR",
                "CODE_SECTEUR_NAF1_LIBELLE",
                "NAF1_LIBELLE_COURT",
            ]
        )["CONSO"]
        .sum()
        .reset_index()
    )
    df_conso_naf["CONSO"] = df_conso_naf["CONSO"].apply(lambda c: round(c))
    df_conso_naf["CONSO_FORMATTED"] = df_conso_naf["CONSO"].apply(
        lambda x: "{:,.0f}".format(x).replace(",", " ")
    )
    nombre_modalites = df_conso_naf['NAF1_LIBELLE_COURT'].nunique()
    
    default_bar_kwargs = dict(
        x="CONSO",
        y="NAF1_LIBELLE_COURT",
        labels={"NAF1_LIBELLE_COURT": "", "CONSO": "Consommation d'électricité (MWh)"},
        title=f"Consommation d'électricité - Secteur <b>{secteur_label}</b> <br><b>{epci_label}</b> (<i>cliquez pour avoir le détail</i>)",
        orientation="h",
        custom_data=[
            "CODE_SECTEUR_NAF1_LIBELLE",
            "CODE_EPCI_LIBELLE",
            "CONSO_FORMATTED",
            "CODE_GRAND_SECTEUR",
        ],
    )

    if code_secteur:  # si on sélectionne un grand secteur
        if click_data_bar:
            # gestion des couleurs - coloration en rouge crimson lorsqu'on clique sur une barre
            code_secteur_naf1 = click_data_bar["points"][0]["customdata"][0]
            df_conso_naf = df_conso_naf.sort_values(by="CONSO", ascending=False).reset_index(
                drop=True
            )
            # Parce qu'il ne faut rentrer que si la donnée du clic est cohérence avec le contenu du bar-plot
            if code_secteur_naf1 in df_conso_naf[
                "CODE_SECTEUR_NAF1_LIBELLE"
            ].values and code_secteur in ("T", "I"):
                df_conso_naf["Code couleur"] = df_conso_naf["CODE_SECTEUR_NAF1_LIBELLE"].apply(
                    lambda x: "Détaillé" if code_secteur_naf1 == x else "Non détaillé"
                )
                default_bar_kwargs |= {
                    "color": "Code couleur",
                    "color_discrete_map": {
                        "Détaillé": "black",
                        "Non détaillé": switcher_grands_secteurs_col[code_secteur],
                    },
                }
            else:
                click_data_bar = None

        # barplot
        fig = px.bar(df_conso_naf, **default_bar_kwargs)

        # Pas de clic pour le secteur R
        if code_secteur == "R":
            fig.update_layout(
                title=f"Consommation d'électricité - Secteur <b>{secteur_label}</b> <br><b>{epci_label}</b>",
            )

        if click_data_bar:
            fig.update_traces(showlegend=False)
        else:
            fig.update_traces(marker_color=switcher_grands_secteurs_col[code_secteur])

    else:  # sinon (1er affichage) on représente tous les codes NAF1, colorés par secteur
        fig = px.bar(
            df_conso_naf,
            **default_bar_kwargs,
            color="CODE_GRAND_SECTEUR",
            color_discrete_map=switcher_grands_secteurs_col,
            category_orders={"CODE_GRAND_SECTEUR": ["A", "I", "T", "R", "X"]},
        )
        # Renommer les étiquettes dans la légende
        fig.for_each_trace(lambda t: t.update(name=switcher_grands_secteurs.get(t.name, t.name)))
        fig.update_layout(legend_title_text="")

    fig.update_traces(
        hovertemplate="<br>".join(
            [
                "<b>%{customdata[0]}</b>",
                "<i>%{customdata[1]}</i>",
                "Consommation : %{customdata[2]} MWh",
            ]
        ),
    )

    fig.update_layout(
        barmode="stack",
        yaxis={"categoryorder": "total ascending"},
        height = 180 + 25 * nombre_modalites,
    )

    # faire une fenêtre de taille proportionnelle au nombre de barres pour ce secteur

    return fig


# Callback pour mettre à jour le 2e Barplot (NAF2) en fonction de la sélection du 1er Barplot (NAF1)
@map_epci.callback(
    Output("bar-plot2", "figure"),
    Input("epci-selector", "value"),
    Input("secteur-selector", "value"),
    Input("bar-plot", "clickData"),
)
def update_bar_plot2(code_epci, code_secteur, click_data_bar):
    if click_data_bar:
        code_secteur_naf1 = click_data_bar["points"][0]["customdata"][0]
    else:
        return initial_blank_figure

    code_epci = code_epci or "200040715"  # GAM
    bar_plot_code_secteur = click_data_bar["points"][0]["customdata"][-1]
    if code_secteur is None and bar_plot_code_secteur is not None:
        code_secteur = bar_plot_code_secteur

    # si on est sur non mentionné, ou sur résidentiel ne pas afficher le barplot (pas de données détaillées par usage)
    # si le sélecteur Secteur et le barplot1 sont différents, on ne doit pas afficher le barplot2
    if (code_secteur != bar_plot_code_secteur) or code_secteur in (None, "X", "R"):
        return initial_blank_figure

    filtered_df = data[
        (data["CODE_EPCI_CODE"] == code_epci)
        & (data["CODE_SECTEUR_NAF1_LIBELLE"] == code_secteur_naf1)
        & (data["CODE_GRAND_SECTEUR"] == code_secteur)
    ]
    try:
        secteur_naf_label = filtered_df["CODE_SECTEUR_NAF1_LIBELLE"].iloc[0]
    except IndexError:
        secteur_naf_label = "Inconnu"

    df_conso_naf = (
        filtered_df.groupby(
            ["CODE_EPCI_LIBELLE", "CODE_SECTEUR_NAF2_LIBELLE", "NAF2_LIBELLE_COURT"]
        )["CONSO"]
        .sum()
        .reset_index()
    )
    df_conso_naf["CONSO"] = df_conso_naf["CONSO"].apply(lambda c: round(c))
    df_conso_naf["CONSO_FORMATTED"] = df_conso_naf["CONSO"].apply(
        lambda x: "{:,.0f}".format(x).replace(",", " ")
    )
    
    nombre_modalites = df_conso_naf['NAF2_LIBELLE_COURT'].nunique()

    fig = px.bar(
        df_conso_naf,
        x="CONSO",
        y="NAF2_LIBELLE_COURT",
        labels={"NAF2_LIBELLE_COURT": "", "CONSO": "Consommation d'électricité (MWh)"},
        title=f"Détail - <b>{secteur_naf_label}</b>",
        orientation="h",
        custom_data=["CODE_SECTEUR_NAF2_LIBELLE", "CODE_EPCI_LIBELLE", "CONSO_FORMATTED"],
    )
    fig.update_traces(
        marker_color=switcher_grands_secteurs_col[code_secteur],
        hovertemplate="<br>".join(
            [
                "<b>%{customdata[0]}</b>",
                "<i>%{customdata[1]}</i>",
                "Consommation : %{customdata[2]} MWh",
            ]
        ),
    )
    fig.update_layout(
        barmode="stack",
        yaxis={"categoryorder": "total ascending"},
        height = 180 + 30 * nombre_modalites,
    )

    # faire une fenêtre de taille proportionnelle au nombre de barres pour ce secteur

    return fig


# MAJ du sélecteur de l'EPCI


@map_epci.callback(
    Output("choropleth-mapbox", "selectedData"),
    Input("choropleth-mapbox", "clickData"),
    State("epci-selector", "value"),
)
def update_selected_data(click_data, dropdown_value):
    if click_data:
        return click_data["points"][0]["customdata"][0]
    return dash.no_update


@map_epci.callback(
    Output("epci-selector", "value"),
    Input("choropleth-mapbox", "selectedData"),
)
def update_dropdown_context(click_map):
    if click_map:
        try:
            return df_conso_gd_secteur_epci[
                df_conso_gd_secteur_epci["CODE_EPCI_LIBELLE"] == click_map
            ]["CODE_EPCI_CODE"].iloc[0]
        except IndexError:
            pass
    return dash.no_update


# MAJ du sélecteur du secteur


@map_epci.callback(
    Output("map-pie-chart", "selectedData"),
    Input("map-pie-chart", "clickData"),
    State("secteur-selector", "value"),
)
def update_selected_data_secteur(click_data, dropdown_value):
    if click_data:
        return click_data["points"][0]["customdata"][-1]
    return dash.no_update


@map_epci.callback(
    Output("secteur-selector", "value"),
    Input("map-pie-chart", "selectedData"),
)
def update_dropdown_context_secteur(click_pie_chart):
    if click_pie_chart:
        return click_pie_chart
    return dash.no_update


if __name__ == "__main__":
    map_epci.run_server(debug=True)
