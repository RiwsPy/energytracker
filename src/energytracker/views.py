import json
import os

from django.conf import settings
from django.http import JsonResponse
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.generic import TemplateView, View

import numpy as np
import pandas as pd
from pandas.compat import numpy

from .graphs import (
    dash_test_energy_map,  # noqa: F401
)

ape_per_epci = pd.read_csv(
    os.path.join(settings.DB_DIR, "ape_per_epci.csv"), sep=";", dtype={"code_epci": str}
)

data_conso = pd.read_csv(
    os.path.join(settings.DB_DIR, "data_conso.csv"),
    sep=";",
    dtype={"CODE_EPCI_CODE": str, "CODE_SECTEUR_NAF2_CODE": str},
)
data_conso["CODE_SECTEUR_NAF2_CODE"] = (
    data_conso["CODE_SECTEUR_NAF2_CODE"]
    .replace(np.nan, "0")
    .apply(lambda x: x.replace(".0", "").zfill(2))
)

naf_levels = pd.read_excel(os.path.join(settings.DB_DIR, "naf2008_5_niveaux.xls"), dtype=str)

naf_desc = json.load(open(os.path.join(settings.DB_DIR, "index_mots_cle_API.json")))
for naf in naf_desc:
    naf["keywords"] = " ".join(naf["keywords"])


class HomeView(TemplateView):
    template_name = "home.html"

    @method_decorator(ensure_csrf_cookie)
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def post(self, request, **kwargs):
        context = self.get_context_data(**kwargs)
        context["compare"] = self.post_context()
        return render(request, "inc/compare-result.html", context)

    def post_context(self, **kwargs):
        context = dict()

        def naf_data(code_naf) -> dict:
            for data in naf_desc:
                if data["id"] == code_naf:
                    return data
            return dict()

        def epci_name(code_epci) -> str:
            if not code_epci:
                return "France"
            return data_conso[data_conso["CODE_EPCI_CODE"] == code_epci]["CODE_EPCI_LIBELLE"].iloc[
                0
            ]

        # def naf_level1_id(code_naf_lv2) -> str:
        #     return naf_levels[naf_levels["NIV2"] == code_naf_lv2]["NIV1"].iloc[0]

        comparative_data = dict()
        if code_naf := self.kwargs.get("pk"):
            code_epci = "200040715"  # FIXME: GAM
            # code_epci = None
            if code_epci:
                df_filtered = ape_per_epci[ape_per_epci["code_epci"] == code_epci]
                df_epci = data_conso[data_conso["CODE_EPCI_CODE"] == code_epci]
            else:
                df_filtered = ape_per_epci
                df_epci = data_conso

            df_by_naf = None
            code_naf_lvX = code_naf
            for _ in range(len(code_naf_lvX)):
                if not code_naf_lvX.endswith("."):
                    df_by_naf = df_filtered["code_naf"].str.startswith(code_naf_lvX)
                    if df_by_naf.size:
                        break
                code_naf_lvX = code_naf_lvX[:-1]

            code_naf_lvX = code_naf_lvX[:2]
            # FIXME: en utilisant directement `df_by_naf` on est pas bon ?
            df_filtered["is_in_naf"] = df_filtered["code_naf"].str.startswith(code_naf_lvX)
            df_in_naf = df_filtered.loc[df_filtered.is_in_naf]
            nb_structure = df_in_naf["count"].sum()

            conso_tot = 0
            conso_ref = 10.027  # foyer 4 personnes : https://particuliers.engie.fr/electricite/conseils-electricite/conseils-tarifs-electricite/consommation-moyenne-electricite-personne.html
            conso_tot = df_epci[df_epci["CODE_SECTEUR_NAF2_CODE"].str.startswith(code_naf_lvX)][
                "CONSO"
            ].sum()

            average_conso = conso_tot / nb_structure
            if average_conso == np.inf:
                average_conso = 0
            nb_reference = average_conso / conso_ref
            comparative_data["nb_structure"] = nb_structure
            comparative_data["conso_tot"] = conso_tot
            # FIXME: hack pour transformer un `nan` en 0
            comparative_data["conso_moyenne"] = max(0, average_conso)
            comparative_data["nb_reference"] = max(0, nb_reference)
            comparative_data["naf"] = naf_data(code_naf_lvX)
            comparative_data["epci_name"] = epci_name(code_epci)

            comparative_data["init_naf"] = naf_data(code_naf)

        return comparative_data

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["data"] = dict()
        # context pour graphs
        # context["data"] = {
        #     "context": {"value": {"naf2": code_naf_lv2, "naf1": naf_level1_id(code_naf_lv2)}}
        # }
        return context


class AboutView(TemplateView):
    template_name = "about.html"


class NafDescView(View):
    def get(self, request, *args, **kwargs):
        # TODO: à mettre en cache serveur
        return JsonResponse({"data": naf_desc})
