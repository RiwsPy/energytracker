import os

from django.conf import settings

import geopandas as gpd

from . import CrossBase


class Cross(CrossBase):
    output_file = "epci_4326.shp"
    help = f"Optimise et transforme EPCI_SHAPEFILE.shp en {output_file}"

    def do(self):
        path = os.path.join(settings.DB_DIR, "EPCI_SHAPEFILE.shp")

        df_epci = gpd.read_file(path)
        df_epci = df_epci.set_crs(epsg=2154, allow_override=True)

        # garder une ligne par EPCI
        df_epci = df_epci.drop_duplicates(["NOM_EPCI"], keep="last")
        df_epci = df_epci.to_crs(4326)

        df_epci[["CODE_EPCI", "NOM_EPCI", "geometry"]].to_file(
            os.path.join(settings.DB_DIR, self.output_file), mode="w"
        )
