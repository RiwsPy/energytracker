# -*- coding: utf-8 -*-
"""
Created on Mon Apr  1 22:01:40 2024

@author: Elise
"""

import os
import pandas as pd



#---IMPORTS ET DATA PREP

path = 'C:/Users/elise/Documents/Data for good/CivicLab - Energy tracker/energytracker/src/db/'

#données EPCI région AuRA détails pour résidentiel et transports
data_secteurs = pd.read_csv(path + "orcae_conso_epci_region_2023-11-24.csv",sep=";", encoding='latin-1')

#données EPCI France détails par code NAF
data_secteurs_naf = pd.read_csv(path + "Donnees-de-consommation-et-de-points-de-livraison-denergie-a-la-maille-EPCI-electricite-.2022-09.csv",
                                sep=";", encoding='utf-8',skiprows=1)



# -- Codes NAF
codes_naf_niv1 = pd.read_excel(os.path.join(path, "naf2008_liste_n1.xls"), skiprows=2)
codes_naf_niv2 = pd.read_excel(os.path.join(path, "naf2008_liste_n2.xls"), skiprows=2)
crspdce_naf = pd.read_excel(os.path.join(path, "naf2008_5_niveaux.xls"))


crspdce_naf_1_2 = crspdce_naf[["NIV2", "NIV1"]].drop_duplicates()
crspdce_naf_1_2["Libellé 1"] = ""
crspdce_naf_1_2["Libellé 2"] = ""
for i, codes in crspdce_naf_1_2.iterrows():
    crspdce_naf_1_2.loc[i, "Libellé 1"] = codes_naf_niv1[codes_naf_niv1["Code"] == codes["NIV1"]][
        "Libellé"
    ].values[0]
    crspdce_naf_1_2.loc[i, "Libellé 2"] = codes_naf_niv2[codes_naf_niv2["Code"] == codes["NIV2"]][
        "Libellé"
    ].values[0]
    

# =============================================================================
# #modifier les intitulés NAF2 trop longs
# crspdce_naf_1_2["Libellé 2"][crspdce_naf_1_2["Libellé 2"].apply(lambda lib: len(lib)) > 60]
# crspdce_naf_1_2["Libellé2_court"] = crspdce_naf_1_2["Libellé 2"].copy()
# crspdce_naf_1_2.loc[
#     crspdce_naf_1_2["Libellé 2"]
#     == "Travail du bois et fabrication d'articles en bois et en liège, à l'exception des meubles ; fabrication d'articles en vannerie et sparterie",
#     "Libellé2_court",
# ] = "Travail du bois, liège, vannerie et sparterie"
# crspdce_naf_1_2.loc[
#     crspdce_naf_1_2["Libellé 2"]
#     == "Fabrication de produits métalliques, à l'exception des machines et des équipements",
#     "Libellé2_court",
# ] = "Fabrication de produits métalliques (hors machines)"
# crspdce_naf_1_2.loc[
#     crspdce_naf_1_2["Libellé 2"]
#     == "Production de films cinématographiques, de vidéo et de programmes de télévision ; enregistrement sonore et édition musicale",
#     "Libellé2_court",
# ] = "Production audio-visuelle"
# crspdce_naf_1_2.loc[
#     crspdce_naf_1_2["Libellé 2"]
#     == "Activités d'architecture et d'ingénierie ; activités de contrôle et analyses techniques",
#     "Libellé2_court",
# ] = "Architecture, ingénierie, contrôle et analyses techniques"
# crspdce_naf_1_2.loc[
#     crspdce_naf_1_2["Libellé 2"]
#     == "Activités des agences de voyage, voyagistes, services de réservation et activités connexes",
#     "Libellé2_court",
# ] = "Agences de voyage et activités connexes"
# crspdce_naf_1_2.loc[
#     crspdce_naf_1_2["Libellé 2"]
#     == "Activités administratives et autres activités de soutien aux entreprises",
#     "Libellé2_court",
# ] = "Activités administratives"
# crspdce_naf_1_2.loc[
#     crspdce_naf_1_2["Libellé 2"]
#     == "Activités indifférenciées des ménages en tant que producteurs de biens et services pour usage propre",
#     "Libellé2_court",
# ] = "Activités des ménages - production de biens et services pour usage propre"
# 
# =============================================================================


#jointure data_secteurs_naf et crspdce_naf_1_2
data_secteurs_naf = data_secteurs_naf.merge(crspdce_naf_1_2[['NIV2','Libellé 1']],how='left',left_on='CODE_SECTEUR_NAF2_CODE',right_on='NIV2')
data_secteurs_naf.rename(columns = {'Libellé 1':'CODE_SECTEUR_NAF1_LIBELLE'},inplace=True)
data_secteurs_naf.drop('NIV2',axis=1,inplace=True)

#associer toute l'industrie manufacturière à l'industrie
data_secteurs_naf.loc[data_secteurs_naf['CODE_SECTEUR_NAF1_LIBELLE']=='Industrie manufacturière','CODE_GRAND_SECTEUR'] = 'I'


#LABEL_GRAND_SECTEUR
def grand_secteur(x):
    match x:
        case 'A':
            return 'Agriculture'
        case 'I':
            return 'Industrie'
        case 'R':
            return 'Résidentiel'
        case 'T':
            return 'Tertiaire'
        case 'X':
            return 'Non mentionné'

data_secteurs_naf['LABEL_GRAND_SECTEUR'] = data_secteurs_naf['CODE_GRAND_SECTEUR'].apply(grand_secteur)


#rassembler les données EPCI AuRA
#data_autres_transports -> 'type de transport'
#data_secteurs -> 'secteur', 'usage'
#data_tertiaire -> 'usage spécifique'
#data_transport_routier -> 'type de transport'
#=> prendre seulement data_secteurs, car pour le reste on n'est pas sûrs de s'il y a des recoupements entre les fichiers

#ne garder que l'électricité et 2022
data_conso_detail = data_secteurs[(data_secteurs['énergie']=='Electricité')&(data_secteurs['année']==2022)]

#valeur en MWh
data_conso_detail.loc[:, 'CONSO'] = data_conso_detail['valeur (GWh)'].apply(lambda c: float(c.replace(',','.'))*1000)

#enlever les lignes "Tous secteurs" et "Tous usages" (totaux)
data_conso_detail = data_conso_detail[data_conso_detail['usage']!='Tous usages']
data_conso_detail = data_conso_detail[data_conso_detail['secteur']!='Tous secteurs']

#sélection des lignes et colonnes à merger (juste résidentiel)
data_conso_detail.drop(['année','énergie','valeur (GWh)','type de données'],axis=1,inplace=True)

data_conso_residentiel = data_conso_detail[data_conso_detail['secteur']=='Résidentiel']
data_conso_residentiel['CODE_GRAND_SECTEUR'] = 'R'

data_conso_residentiel.rename(columns = {'code territoire':'CODE_EPCI_CODE', 'nom territoire':'CODE_EPCI_LIBELLE', 'secteur':'LABEL_GRAND_SECTEUR', 'usage':'CODE_SECTEUR_NAF1_LIBELLE'}, inplace=True)

#dataset par code NAF : on enlève les lignes pour le résidentiel en AuRA pour les remplacer
codes_epci_detail = data_conso_residentiel['CODE_EPCI_CODE'].unique()
data_secteurs_naf = data_secteurs_naf[~((data_secteurs_naf['CODE_GRAND_SECTEUR']=='R')&(data_secteurs_naf['CODE_EPCI_CODE'].isin(codes_epci_detail)))]


#-concaténer les 2 dataframes (on rempace les données du résidentiel)
data_conso = pd.concat([data_secteurs_naf,data_conso_residentiel])

data_conso = data_conso[['CODE_EPCI_CODE','CODE_EPCI_LIBELLE','CODE_SECTEUR_NAF2_CODE', 'CODE_SECTEUR_NAF2_LIBELLE', 
                         'CODE_SECTEUR_NAF1_LIBELLE','CODE_GRAND_SECTEUR', 'LABEL_GRAND_SECTEUR', 'CONSO']]

data_conso = data_conso[data_conso["CONSO"]!='secret']


#-version courte des libellés pour les barplot
def format_code(code):
    if pd.isnull(code) or len(str(code)) <= 35:
        return code
    else:
        return code[0:32] + '...'

data_conso['NAF1_LIBELLE_COURT'] = data_conso['CODE_SECTEUR_NAF1_LIBELLE'].apply(format_code)
data_conso['NAF2_LIBELLE_COURT'] = data_conso['CODE_SECTEUR_NAF2_LIBELLE'].apply(format_code)

data_conso.to_csv("C:/Users/elise/Documents/Data for good/CivicLab - Energy tracker/energytracker/src/db/data_conso.csv",sep=";",index=False)

#save codes EPCI de la région Aura
pd.DataFrame(codes_epci_detail).to_csv("C:/Users/elise/Documents/Data for good/CivicLab - Energy tracker/energytracker/src/db/liste_epci_aura.csv",sep=";",header=False,index=False)
