import os

from django.conf import settings


class CrossBase:
    output_file = ""

    def __init__(self, **kwargs):
        self.options = kwargs

    def do(self):
        raise NotImplementedError

    def output(self, df, **kwargs):
        kwargs = {"sep": ";"} | kwargs
        with open(os.path.join(settings.DB_DIR, self.output_file), "w+") as f:
            df.to_csv(f, **kwargs)
