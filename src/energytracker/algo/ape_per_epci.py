from operator import index
import os

from django.conf import settings

import pandas as pd

from . import CrossBase


class Cross(CrossBase):
    help = "Compte le nombre de code APE par EPCI"
    output_file = "ape_per_epci.csv"

    def do(self):
        path = os.path.join(
            settings.BASE_DIR.parent, "perso", "StockEtablissementActif_utf8_geo.csv"
        )
        # On récupère la correspondance des codes EPCI <-> codes communes
        code_commune_to_epci = pd.read_csv(
            os.path.join(settings.DB_DIR, "code_commune_to_epci.csv"),
            sep=";",
            dtype=str,
        )

        mapping = dict(
            zip(code_commune_to_epci["code_commune"], code_commune_to_epci["code_epci"])
        )

        col_names = {
            "codeCommuneEtablissement": "code_commune",
            "activitePrincipaleEtablissement": "code_naf",
        }

        # On récupère l'intégralité des entreprises qui ont un code APE et un code commune
        apes = (
            pd.read_csv(path, sep=",", usecols=col_names.keys(), dtype=str)
            .dropna()
            .rename(columns=col_names)
        )
        apes["code_epci"] = apes["code_commune"].map(mapping)
        apes = apes.dropna()

        # Sinon partir sur :
        count_df = apes.groupby(["code_epci", "code_naf"]).size().reset_index(name="count")

        # Facultatif, pas sûr que la gestion en 2D soit optimisé pour le traitement
        # pivot_table = (
        #     count_df.pivot(index="code_epci", columns="code_naf", values="count")
        #     .fillna(0)
        #     .astype(int)
        # )
        self.output(count_df, index=False)
