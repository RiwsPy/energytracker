import os

from django.conf import settings

import numpy as np
import pandas as pd

from . import CrossBase


class Cross(CrossBase):
    output_file = "data_conso.csv"

    def do(self):
        path = settings.DB_DIR

        codes_naf_niv1 = pd.read_excel(os.path.join(path, "naf2008_liste_n1.xls"), skiprows=2)
        codes_naf_niv2 = pd.read_excel(os.path.join(path, "naf2008_liste_n2.xls"), skiprows=2)
        crspdce_naf = pd.read_excel(os.path.join(path, "naf2008_5_niveaux.xls"))

        codes_naf_niv1["Libellé_court"] = codes_naf_niv1["Libellé"].replace(
            {
                "Production et distribution d'électricité, de gaz, de vapeur et d'air conditionné": "Production et distribution d'énergie",
                "Production et distribution d'eau ; assainissement, gestion des déchets et dépollution": "Gestion de l'eau et des déchets",
                "Activités des ménages en tant qu'employeurs ; activités indifférenciées des ménages en tant que producteurs de biens et services pour usage propre": "Activités des ménages",
            }
        )

        crspdce_naf_1_2 = crspdce_naf[["NIV2", "NIV1"]].drop_duplicates()
        label_1 = codes_naf_niv1.set_index("Code")["Libellé_court"]
        label_2 = codes_naf_niv2.set_index("Code")["Libellé"]

        crspdce_naf_1_2["Libellé 1"] = crspdce_naf_1_2["NIV1"].map(label_1)
        crspdce_naf_1_2["Libellé 2"] = crspdce_naf_1_2["NIV2"].map(label_2)

        data = pd.read_csv(
            os.path.join(
                path,
                "Donnees-de-consommation-et-de-points-de-livraison-denergie-a-la-maille-EPCI-electricite-.2022-09.csv",
            ),
            sep=";",
            skiprows=1,
            dtype={"CODE_EPCI_CODE": str},
        )

        data["CONSO"] = data["CONSO"].replace({"secret": np.nan}).astype(float)
        # data["CONSO"] = data["CONSO"].apply(lambda c: np.nan if c == "secret" else float(c))

        # ajout du libellé du code NAF niveau 1
        data["CODE_SECTEUR_NAF1_LIBELLE"] = np.nan
        for i, row in data.iterrows():
            if not pd.isnull(row["CODE_SECTEUR_NAF2_CODE"]):
                data.loc[i, "CODE_SECTEUR_NAF1_LIBELLE"] = crspdce_naf_1_2[
                    crspdce_naf_1_2["NIV2"] == row["CODE_SECTEUR_NAF2_CODE"]
                ]["Libellé 1"].values[0]

        switcher_grands_secteurs = {
            "A": "Agriculture",
            "I": "Industrie",
            "R": "Résidentiel",
            "T": "Tertiaire",
            "X": "Non mentionné",
        }
        data["LABEL_GRAND_SECTEUR"] = data["CODE_GRAND_SECTEUR"].map(switcher_grands_secteurs)

        self.output(data, index=False)
