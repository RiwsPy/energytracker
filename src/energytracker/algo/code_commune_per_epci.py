import os

from django.conf import settings

import pandas as pd

from . import CrossBase


class Cross(CrossBase):
    help = "Associe les codes EPCI aux codes communes"
    output_file = "code_commune_to_epci.csv"

    def do(self):
        path = os.path.join(settings.BASE_DIR.parent, "perso", "georef-france-commune.csv")
        col_names = {
            "Code Officiel Commune": "code_commune",
            # "Nom Officiel EPCI": "name",
            "Code Officiel EPCI": "code_epci",
        }
        df = pd.read_csv(path, sep=";", usecols=col_names.keys(), dtype=str).rename(
            columns=col_names
        )
        # grouped_df = (
        #     df.groupby(["Code Officiel EPCI", "Nom Officiel EPCI"])["Code Officiel Commune"]
        #     .agg(list)
        #     .reset_index()
        # ).rename(columns=col_names)

        self.output(df, index=False)
