## Architecture de base
* `src/`: contient le projet en tant que tel
    * `db/` : contient les fichiers data
    * `energytracker/` : contient les fichiers de l'application
        * `graphs/` : le terrain de jeu de `dash`
        * `migrations/` : fichiers versionnant les différentes évolutions de la db, ces fichiers sont automatiquement générés avec les commandes `makemigrations` et `migrate` ⇒ pas touche !
        * `static/` : contient les fichiers statics (js, css, img etc)
        * `templates/` : contient les fichiers html
        * `urls.py` : gère les différentes urls de l'application
        * `views.py` : contient les vues de l'application
    * `project/` : contient les fichiers de configuration du projet
    * `manage.py` : fichier à exécuter pour lancer commandes django
    * `db.sqlite3` : la vraie db (utilisée au minimum)
* `.gitignore`: les fichiers non versionnés
* `pyproject.toml`: contient la configuration de quelques outils utiles pour le projet
* `requirements.txt`: contient les dépendances du projet ainsi que leur numéro de version


Étant donné que l'on a tous une expérience et des habitudes variées, il convient de rappeler certains principes.

## De grands pouvoirs impliquent de grandes responsabilités
Ce repo est commun à toute l'équipe, pensez-y avant de pusher vos modifications `wip`
* Si vous êtes assez à l'aise avec git mais pas suffisemment pour pusher les yeux fermés, vous pouvez forker le projet et proposer vos modifications par PR
* La plupart des modifications se situeront dans le dossier `src`. Dans le cas contraire : demandez/prévenez sur le Discord
* La taille du repo est limitée (à définir), attention aux fichiers volumineux (notamment data)


## Back-end - Python

### Python 3.11

La version python officiellement supportée est la 3.11, c'est compatible avec des versions python inférieures (3.9 et 3.10), mais on ne peut pas l'assurer. Dans le doute, préviligiez la version 3.11.\
Les versions 3.12+ ne seront pas compatibles à court terme.


### PEP8
Il y a des conventions pour faciliter la lecture et la compréhension du code, notamment explicitées dans la `PEP8`.
Il est conseillé de respecter ces conventions. Il y a des outils pour vous aider mais cela dépendre de votre IDE, voici quelques exemples :
* [flake8](https://pypi.org/project/flake8/) : vous renseigne sur le code ne respectant pas la PEP8
* [black](https://pypi.org/project/black/) : réécrit votre code sans compromis de façon à respecter la PEP8
* [ruff](https://pypi.org/project/ruff/) : comme black, mais propose également de trier les imports

Le nombre de caractère par ligne est accepté jusqu'à 99.

`black` et `ruff` ont une pré-configuration minimale dans le `pyproject.toml`.\
Si besoin, vous pouvez également compléter ce fichier. Attention à ne pas surcharger/modifier/supprimer les configurations déjà présentes.


## Front-end

Technos à définir.\
Utilisation de django dans les templates à définir.
