![EnergieTracker](/src/energytracker/static/img/energietracker.svg)

CivicLab #4 - Thème `Énergie en partage`



## Requirements

* python>=3.10,<=3.11
* pip


## Getting started

### Virtual Env

#### Création
```bash
python -m venv python-env
```

#### Activation
```bash
source python-env/bin/activate
```

#### Installation
```bash
pip install -r requirements.txt
```



### Django

Les commandes Django se lancent grâce au fichier `manage.py` dans le dossier `src/`
```bash
cd src/
```

(Facultatif) Il est possible d'un message de warning s'affiche `You have X unapplied migration(s).` Il suffit de suivre les instructions :
```bash
python -m manage.py migrate
```

Enfin, il faut lancer le serveur :
```bash
python -m manage.py runserver
```

L'url par défaut est la suivante :\
`http://127.0.0.1:8000/`

